//
//  main.m
//  MaxSlice
//
//  Created by Mark Eissler on 10/1/15.
//  Copyright © 2015 Mark Eissler. All rights reserved.
//

#import <Foundation/Foundation.h>

// You are asked to write a function called max_slice which takes 
// as an input an array of integers, and returns the slice with
// the largest sum of the elements.
//
// series = [10, 12, 1, 3, 19, -400, 222, 300, 245, -19, 323];
//
// output = [222, 300, 245, -19, 323]
//

#define DEBUG_CYCLE

typedef struct {
  NSUInteger cycle;
  NSInteger total;
} slicerec;

void build_slice(NSMutableDictionary *slicedList, NSArray *inputList,
                NSUInteger start, NSUInteger end, NSInteger total, slicerec *sliceRec)
{
  NSArray *slice = nil;

  if (end >= start && end < inputList.count)
  {
    slice = [inputList subarrayWithRange:NSMakeRange(start, (end - start) + 1)];
    NSInteger newTotal = total + [[slice lastObject] integerValue];
    [slicedList setObject:slice
                   forKey:[NSString stringWithFormat:@"%ld_%ld", start,
                           newTotal]];
    if (sliceRec->cycle == NSNotFound || sliceRec->total < newTotal) {
      sliceRec->cycle = start;
      sliceRec->total = newTotal;
    }
    build_slice(slicedList, inputList, start, ++end, newTotal, sliceRec);
  } else if (start < inputList.count) {
    NSUInteger newStart = start + 1;
    NSInteger newTotal = 0;
#ifdef DEBUG_CYCLE
    NSLog(@" ");
    NSLog(@"-- newcycle: %ld", newStart);
#endif
    build_slice(slicedList, inputList, newStart, newStart, newTotal, sliceRec);
  }

  return;
}

NSArray *max_slice(NSArray *integerList)
{
  NSMutableDictionary *slicedList = [[NSMutableDictionary alloc] init];
  slicerec sliceRec = { NSNotFound, NSNotFound };
  build_slice(slicedList, integerList, 0, 0, 0, &sliceRec);

  NSString *maxSliceKey =
    [NSString stringWithFormat:@"%ld_%ld", sliceRec.cycle, sliceRec.total];

#ifdef DEBUG_CYCLE
  // output
  NSLog(@" ");
  NSLog(@"maxSlice total: %ld, cycle: %ld", sliceRec.total, sliceRec.cycle);
  NSLog(@"maxSlice index: %ld_%ld", sliceRec.cycle, sliceRec.total);
  NSLog(@"maxSlice: %@", slicedList[maxSliceKey]);
#endif

  return slicedList[maxSliceKey];
}

int main(int argc, const char *argv[])
{
  @autoreleasepool {
    // define input
    NSArray *inputList =
      @[@10, @12, @1, @3, @19, @-400, @222, @300, @245, @ - 19, @323];
    NSLog(@"inputList: %@", inputList);

    // do it
    NSArray *largestSlice = max_slice(inputList);
    
    // output
    NSLog(@"outputList: %@", largestSlice);
  }
  
  return 0;
}
