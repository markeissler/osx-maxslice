# MaxSlice
Given a list of integers:

```
    [10, 12, 1, 3, 19, -400, 222, 300, 245, -19, 323]
```

Write a function called `max_slice` which takes the array of integers as input and returns the slice with the largest some of the elements.

For example, for the above list of integers output should be:

```
[222, 300, 245, -19, 323]
```

## Optimization and Design
While this does appear to be a brute force solution, the design translates to an O(n) solution through the use of a recursive function (`build_slice`) that is called fewer times throughout the run. The `build_slice` function generates each slice and stores the result in a dictionary, while the index and total of the current max_slice is stored in a struct (`slicerec`). The dictionary is maintained to allow retrieval of the max_slice integers later but, in hindsight, the dictionary could be eliminated all together to decrease space complexity...the max_slice elements could be stored in an array passed between recursive calls or even an instance of a custom class, which could eliminate the struct as well.

##License
PointTest is licensed under the Creative Commons (CC0) 1.0 Universal license.
